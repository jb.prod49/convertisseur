import xml.etree.ElementTree as ET
import pandas as pd
import sqlite3
import os
import pathlib

#--------------------------------------#
#---------- Fonction convert ----------#
#--------------------------------------#

#---------- fonction read SQLite ----------# 
def sql_to_dataframes(sql_filepath):
    conn = sqlite3.connect(':memory:')
    cursor = conn.cursor()
    query = "SELECT name FROM sqlite_master WHERE type='table';"
    tables = pd.read_sql_query(query, conn)
    dataframes = {}
    for table_name in tables['name']:
        df = pd.read_sql(f"SELECT * FROM {table_name}", conn)
        dataframes[table_name] = df
    conn.close()
    return dataframes   
          
#---------- fonction read ----------# 
def read_data(input_filepath: str) -> pd.DataFrame:
    path = pathlib.Path(input_filepath)
    file_ext = os.path.splitext(input_filepath)[1]
    if file_ext == '.csv':
        return pd.read_csv(input_filepath)
    elif file_ext == '.xlsx':
         df = pd.read_excel(input_filepath)
         df.columns = [f"col_{i}" if col.startswith('Unnamed') else col for i, col in enumerate(df.columns)]
         return df
    elif file_ext == '.json':
        return pd.read_json(input_filepath)
    elif file_ext == '.xml':
        return pd.read_xml(input_filepath)
    elif file_ext ==".sql":   
        return sql_to_dataframes(input_filepath)
    else:
        raise ValueError("Unsupported file format")
    
#---------- fonction write_data ----------# 
def write_data(df: pd.DataFrame, out_filepath: str, format: str) -> None:
    if format == 'csv':
        df.to_csv(out_filepath, index=False)
    elif format == 'json':
        df.to_json(out_filepath, orient='records')
    elif format == 'xlsx':
        df.to_excel(out_filepath, index=False)
    elif format == 'xml':
        df.to_xml(out_filepath, index=True)
    elif format == 'sql':
        base_name = os.path.basename(out_filepath)
        table_name = os.path.splitext(base_name)[0].replace('-', '_').replace(' ', '_')
        conn = sqlite3.connect(':memory:')
        df.to_sql(table_name, conn, if_exists='replace', index=False)
        with open(out_filepath, 'w') as f:
            for line in conn.iterdump():
                f.write('%s\n' % line)
        conn.close()
    else:
        raise ValueError("Unsupported output format")

#---------- fonction convert ----------#   
def convert(input_filepath, file_type, out_filepath = None):
    if not out_filepath:
        out_filepath = os.path.splitext(input_filepath)[0]
        out_filepath = "out_file/"+out_filepath.split("/")[1]+"."+file_type
        
    dataframe = read_data(input_filepath) 
    return write_data(dataframe, out_filepath, file_type)
