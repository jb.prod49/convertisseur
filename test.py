from main import convert

path_xlsx = "input_file/test.xlsx"
path_json = "input_file/products_date.json"
path_csv = "input_file/employees.csv"
path_xlm = "input_file/data-avion.xml"
path_sql = "input_file/employees_2.sql"

# ---------- csv to ? ----------#
convert(path_csv, 'json')
convert(path_csv, 'xlsx')
convert(path_csv, 'xml')
convert(path_csv, 'sql')

#---------- excel to ? ----------#
convert(path_xlsx, 'csv')
convert(path_xlsx, 'json')
convert(path_xlsx, 'xml')
convert(path_xlsx, 'sql')

#---------- json to ? ----------#  
convert(path_json, 'csv')
convert(path_json, 'xlsx')
convert(path_json, 'xml')
convert(path_json, 'sql')

#---------- xml to ? ----------#
convert(path_xlm, 'csv')
convert(path_xlm, 'json')
convert(path_xlm, 'xlsx')
convert(path_xlm, 'sql')

# ---------- sql to ? ----------#
convert(path_sql, 'csv')
convert(path_sql, 'json')
convert(path_sql, 'xlsx')
convert(path_sql, 'xml')
